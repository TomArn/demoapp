"""
App to test things out
"""
import toga
from toga.style import Pack
from toga.style.pack import COLUMN, ROW
from com.t_arn.pymod.ui.window import TaGui


class DemoApp(toga.App):

    def startup(self):
        mygui = MainGui(self, None, title=self.formal_name)
        mygui.show()


class MainGui(TaGui):
    app = None
    main_box = None
    window = None

    def __init__(self, app, parentGui, title, **kwargs):
        super().__init__(app, parentGui, title, **kwargs)
    # __init__

    def build_gui(self):
        self.main_box = toga.Box(style=Pack(direction=COLUMN))
        label = toga.Label("This is a demo app to test things out.")
        self.main_box.add(label)
        button = toga.Button("Do something", on_press=self.handle_btn_action)
        self.main_box.add(button)

    def handle_btn_action(self, widget):
        self.window.info_dialog("info", toga.platform.current_platform)


def main():
    return DemoApp()
