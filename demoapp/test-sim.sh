#!/bin/bash
briefcase update ios
if [ $? != 0 ]; then
  exit $?
fi
briefcase build iOS -d "iPhone 8::iOS 14.4"
if [ $? != 0 ]; then
  exit $?
fi
briefcase run iOS -d "iPhone 8::iOS 14.4"
